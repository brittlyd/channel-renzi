clear; close all; clc;
% main_dir = '/home/custer/Dropbox (MREL)/MATLAB/Channel - Renzi';
main_dir = 'C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi Data';
filename_prefix = 'flap_pressure';
cd(main_dir)
mkdir(filename_prefix);
folder = strcat(main_dir,'/',filename_prefix);
cd(folder)

%% Define parameters of sums and solving process
P = 4; % Upper limit of sums
N = 4; % Number of modes considered
M = 4;
u_num = 100;
j = 0:P; 
n = 0:N-1; % Vector of modes
v0j = cos(((2*j+1)*pi)/((2*P)+2)); % Calculate evaluation points
J = 100; % Upper limit for sub-series

%%%% Set simulation params structure
SIM_params.P = P;
SIM_params.N = N;
SIM_params.u_num = u_num;
SIM_params.M = M;
SIM_params.n = n;
SIM_params.v0j = v0j;
SIM_params.J = J;

%% Dimenionsional parameters
%%% Define wave and ocean paramenters (full-scale, dimentional) and fill
%%% WAVE_params structure
g = 9.81;
rho = 1000;
h1 = 0.7; % Depth of water

A01 = 0.1/2; % Incident wave amplitude [m] (between 5 and 15 cm)

T1 = 2.0; %4:14; % Period of wave
omega1 = 2*pi/T1; % Frequency of wave

%%%% Set WAVE params1 structure
WAVE_params1.g = g;
WAVE_params1.rho = rho;
WAVE_params1.h1 = h1;
WAVE_params1.A01 = A01;
WAVE_params1.T1 = T1;
WAVE_params1.omega1 = omega1;

%%% Define OSWEC parameters (full-scale, dimensional)
w1 = 0.85; % Flap width
c1 = 0.115; % Height of foundation
a1 = 0.1365; % Flap thickness
m_f = 18.7; % Mass of flap
% m_w = 45; % Mass of displaced water (fully submerged)
% Vol = 0.04584; % Volume of flap
% Fg = m_f*g;
% Fb = m_w*g;
% db = 0.230; % Distance along flap to center of buoyancy
dg = 0.34; % Distance along flap to center of mass
S1 = m_f*dg; % First moment of inertia (kg m)
C1 = (rho*w1*a1*(h1-c1)^2)-S1;
% C1 = -(Fb*db-Fg*dg);
I1 = 0.543; % kg m^2
b1 = 0.9; % Channel width

%%%% Set OWSC params1 structure
OWSC_params1.w1 = w1;
OWSC_params1.c1 = c1;
OWSC_params1.C1 = C1;
OWSC_params1.I1 = I1;
OWSC_params1.b1 = b1;

A1 = b1/10; % I don't actually know what this is? Amplitude scale, must be much smaller than w1
WAVE_params1.A1 = A1;
eps = A1/b1;
WAVE_params1.eps = eps;

%% Define time and space
xnum = 1;
ynum = 20;
znum = 20;
tnum = 40;

%%% Dimensional time and space
x1 = 0; % linspace(-0.2,0.2,xnum);
y1 = linspace(-0.5*w1,0.5*w1,ynum);
z1 = linspace(-h1,0,znum);
t1 = linspace(0,2*T1,tnum);

%%% Set dimensional structure
XT_params1.x1 = x1;
XT_params1.y1 = y1;
XT_params1.z1 = z1;
XT_params1.t1 = t1;

%% Nondimensionalize parameters
[OWSC_params,WAVE_params,XT_params] = nondimensionalize_channel(OWSC_params1,WAVE_params1,XT_params1);

%% Get wavenumbers
[WAVE_params] = getKappa(SIM_params,WAVE_params);

%% Get Alpha and Beta
[COEFF_params] = getAlphaBeta_channel(SIM_params,OWSC_params,WAVE_params);

%% Get Hydro params (1)
[HYDRO_params] = getHydro_channel(OWSC_params,WAVE_params,COEFF_params);

%% PTO
C1_PTO = 0;
nu1_PTO = 0;
mu1_PTO = 0;

C_PTO = C1_PTO/(rho*b1^4);
nu_PTO = nu1_PTO/(rho*b1^5*sqrt(g/b1));
mu_PTO = mu1_PTO/(rho*b1^5);

HYDRO_params.nu_PTO = nu_PTO;
HYDRO_params.C_PTO = C_PTO;
HYDRO_params.mu_PTO = mu_PTO;

%% Get Hydro params
[HYDRO_params] = getHydro2_channel(HYDRO_params,OWSC_params,...
    WAVE_params,XT_params);
[HYDRO_params1] = dimensionalizeHydro(HYDRO_params,OWSC_params1,WAVE_params1);

%% Calculate torques
[TORQUES_params] = getTorques(WAVE_params,HYDRO_params,OWSC_params);
[TORQUES_params1] = dimensionalizeTorques(TORQUES_params,OWSC_params1,...
    WAVE_params1,XT_params1);

HYDRO_params1.nu1_PTO = nu1_PTO;
HYDRO_params1.C1_PTO = C1_PTO;
HYDRO_params1.mu1_PTO = mu1_PTO;

%%
%%% Initialize nondimensional potentials
Phi_I = zeros(znum,xnum,ynum);
Phi_R = zeros(znum,xnum,ynum);
Phi_D = zeros(znum,xnum,ynum);
PHI = zeros(znum,xnum,ynum,tnum);
PHI_I = zeros(znum,xnum,ynum,tnum);
PHI_R = zeros(znum,xnum,ynum,tnum);
PHI_D = zeros(znum,xnum,ynum,tnum);

Phi_I1 = zeros(znum,xnum,ynum);
Phi_R1 = zeros(znum,xnum,ynum);
Phi_D1 = zeros(znum,xnum,ynum);
PHI1 = zeros(znum,xnum,ynum,tnum);
PHI_I1 = zeros(znum,xnum,ynum,tnum);
PHI_R1 = zeros(znum,xnum,ynum,tnum);
PHI_D1 = zeros(znum,xnum,ynum,tnum);

tic
for yy = 1:ynum
    for zz = 1:znum
        for xx = 1:xnum
            POINT_params.x = XT_params.x(xx);
            POINT_params.y = XT_params.y(yy);
            POINT_params.z = XT_params.z(zz);
            POINT_params.t = XT_params.t;
            [PHI_params] = getPHI_channel(SIM_params,OWSC_params,...
                    WAVE_params,COEFF_params,HYDRO_params,POINT_params);

            %%% Spatial
            Phi_I(zz,xx,yy) = PHI_params.Phi_I;
            Phi_R(zz,xx,yy) = PHI_params.Phi_R;
            Phi_D(zz,xx,yy) = PHI_params.Phi_D; 
            
            %%% Spatiotemporal
            PHI(zz,xx,yy,:) = PHI_params.PHI;
            PHI_I(zz,xx,yy,:) = PHI_params.PHI_I;
            PHI_R(zz,xx,yy,:) = PHI_params.PHI_R;
            PHI_D(zz,xx,yy,:) = PHI_params.PHI_D;
            
            %%% Dimensionalize PHI
            Phi_I1(zz,xx,yy) = Phi_I(zz,xx,yy)*sqrt(g*b1)*A1;
            Phi_R1(zz,xx,yy) = Phi_R(zz,xx,yy)*sqrt(g*b1)*A1;
            Phi_D1(zz,xx,yy) = Phi_D(zz,xx,yy)*sqrt(g*b1)*A1;

            PHI_I1(zz,xx,yy,:) = Phi_I1(zz,xx,yy).*exp(-1i.*omega1.*t1);
            PHI_R1(zz,xx,yy,:) = Phi_R1(zz,xx,yy).*exp(-1i.*omega1.*t1);
            PHI_D1(zz,xx,yy,:) = Phi_D1(zz,xx,yy).*exp(-1i.*omega1.*t1);
        end
    end
    
    yy
    toc
end

PHI1 = (PHI_I1+PHI_R1+PHI_D1);

PHI_params1.PHI1 = PHI1;
PHI_params1.PHI_I1 = PHI_I1;
PHI_params1.PHI_R1 = PHI_R1;
PHI_params1.PHI_D1 = PHI_D1;

%% Calculate pressure
[PRESSURE_params1] = getPressure(WAVE_params1,PHI_params1);
 
%% Calculate eta
[ETA_params] = getEta(WAVE_params1,PHI_params1);

%% Calculate surge force
[FORCES_params] = getSurgeForce(XT_params,SIM_params,OWSC_params,...
    WAVE_params,COEFF_params,HYDRO_params);
[FORCES_params1] = dimensionalizeSurgeForce(FORCES_params,...
    WAVE_params1,OWSC_params1,XT_params1);
%%
save(filename_prefix)
