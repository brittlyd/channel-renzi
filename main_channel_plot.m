clear; close all; clc;
% main_dir = '/home/custer/Dropbox (MREL)/MATLAB/Channel - Renzi'; %'C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi';
main_dir = 'C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi Data';
filename_prefix = 'openfoam_compare'; %'channel_pressure_test';
folder = strcat(main_dir,'/',filename_prefix);
cd(folder)
load(strcat(filename_prefix,'.mat'));

%% Extract parameters
pressure1 = PRESSURE_params1.pressure1; % flap pressure
eta = ETA_params.eta; % total wave elevation
eta_I = ETA_params.eta_I; % wave elevation due to incident potential
eta_R = ETA_params.eta_R; % due to radiative potential
eta_D = ETA_params.eta_D; % due to diffractive potential

Fe1 = real(TORQUES_params1.Fe1); % excitation torque
Fr1 = real(TORQUES_params1.Fr1); % radiation torque
Ft1 = real(TORQUES_params1.Ft1); % total torque
Fb1 = real(TORQUES_params1.Fb1); % buoyancy torque
Fpto1 = real(TORQUES_params1.Fpto1); % PTO torque

theta1 = real(HYDRO_params1.theta1); % theta in radians
theta1_deg = theta1*180/pi; % theta in degrees
v1 = real(HYDRO_params1.v1); % angular velocity

Pm = v1.*Fpto1; % mechanical power

%% Define y position to plot, x index for flap
y_ind = 1;
[x_min,x_ind] = min(abs(x1));

%% Figure of flap with wave and pressure field (GIF)
fh1 = w1/2+0.5;
f1 = figure(1);
set(gcf, 'Position', [100 100 2000 200])
axis tight manual % this ensures that getframe() returns a consistent size
% axis equal
filename_GIF = strcat(filename_prefix,'_FlapMotion.gif');
dt = t1(2)-t1(1);
depth = -0.7;
for tt = 1:tnum
    xlim([-4 4])
    ylim([depth 0.5])
    plot([-4 -4],[depth 0.44],'k','linewidth',1.5)
    hold on
    plot([4 4],[depth 0.44],'k','linewidth',1.5)
    plot([-4 4],[depth depth],'k','linewidth',1.5)
    plot([-4 4],[0 0],'b--')
        
    pcolor(x1,z1,real(pressure1(:,:,y_ind,tt)))
%     plot(x1,eta_I(:,:,y_ind,tt),'go-','linewidth',1.5)
%     plot(x1,eta_R(:,:,y_ind,tt),'ro-','linewidth',1.5)
%     plot(x1,eta_D(:,:,y_ind,tt),'bo-','linewidth',1.5)
    plot(x1,eta(:,:,y_ind,tt),'ko-','linewidth',1.5)

    caxis([min(min(min(min(real(pressure1))))) max(max(max(max(real(pressure1)))))]);
    shading interp
    hhh = colorbar;
    ylabel(hhh, 'Pressure [Pa]')

    x0 = 0;
    x2 = fh1*sin(theta1(tt));
   
    y0 = c1-h1;
    y2 = c1+(fh1*cos(theta1(tt)))-h1;
   
    hold on
    plot([0 0],[-h1 c1-h1],'k','linewidth',2)
    plot([x0, x2],[y0, y2],'k','linewidth',2)
    plot(0,c1-h1,'ko','linewidth',2)
    
    x1_mat = repmat(x1,length(z1),1);
    z1_mat = repmat(z1.',1,length(x1));
%     plot(x1_mat,z1_mat,'ko','linewidth',1.5)
%     xlim([x1(1) x1(end)])
    ylim([-h1 1])


   drawnow

   % Capture the plot as an image 
   frame = getframe(f1); 
   im = frame2im(frame); 
   [imind,cm] = rgb2ind(im,256); 

   % Write to the GIF File 
   if tt == 1 
       imwrite(imind,cm,filename_GIF,'gif', 'Loopcount',inf,'DelayTime',dt); 
   else 
       imwrite(imind,cm,filename_GIF,'gif','WriteMode','append','DelayTime',dt); 
   end     

    pause(dt)
    clf
end

%% Time series of torques
figure(2)
plot(t1,Fe1,'linewidth',1.5)
hold on
plot(t1,Fr1,'linewidth',1.5)
plot(t1,Fb1,'linewidth',1.5)
plot(t1,Fpto1,'linewidth',1.5)
plot(t1,Ft1,'linewidth',1.5)
xlabel('t^1 [s]')
ylabel('F [n]')
legend('F_e','F_r','F_b','F_{PTO}','F_{total}')


saveas(gcf,strcat(filename_prefix,'_forces','.png'))

%% Wave elevation decomposition time series
f3 = figure(3);

% axis tight manual % this ensures that getframe() returns a consistent size
axis equal
filename_GIF = strcat(filename_prefix,'_eta.gif');
dt = t1(2)-t1(1);
for tt = 1:tnum

    plot(x1,eta_I(:,:,y_ind,tt),'o-','linewidth',1.5)
    hold on
    plot(x1,eta_R(:,:,y_ind,tt),'o-','linewidth',1.5)
    plot(x1,eta_D(:,:,y_ind,tt),'o-','linewidth',1.5)
    plot(x1,eta(:,:,y_ind,tt),'o-','linewidth',1.5)
    ylim([-0.1 0.1])
    
    legend('\eta_I','\eta_R','\eta_D','\eta')
    
    drawnow

    % Capture the plot as an image 
    frame = getframe(f3); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 

    % Write to the GIF File 
    if tt == 1 
       imwrite(imind,cm,filename_GIF,'gif', 'Loopcount',inf,'DelayTime',dt); 
    else 
       imwrite(imind,cm,filename_GIF,'gif','WriteMode','append','DelayTime',dt); 
    end  
    
    pause(t1(2)-t1(1))
    clf
end

%% Flap pressure
f4 = figure(4);
axis tight manual % this ensures that getframe() returns a consistent size
filename_GIF = strcat(filename_prefix,'_flap_pressure.gif');
pmax = max(max(max(max(pressure1))));
dt = t1(2)-t1(1);
for tt = 1:tnum
p1 = reshape(pressure1(:,x_ind,:,tt),[znum,ynum]);
pcolor(y1,z1,p1)
shading interp
xlabel('y1')
ylabel('z1')
title(strcat('time = ',num2str(t1(tt))))
colorbar
caxis([-pmax pmax])

drawnow

% Capture the plot as an image 
frame = getframe(f4); 
im = frame2im(frame); 
[imind,cm] = rgb2ind(im,256); 

% Write to the GIF File 
if tt == 1 
   imwrite(imind,cm,filename_GIF,'gif', 'Loopcount',inf,'DelayTime',dt); 
else 
   imwrite(imind,cm,filename_GIF,'gif','WriteMode','append','DelayTime',dt); 
end   
   
pause(dt)
clf
end

%% Mechanical power
figure(5)
grid
subplot(3,1,1)
plot(t1,theta1_deg,'linewidth',1.5)
xlabel('t^1 (sec)')
ylabel('\theta^1 (deg)')

subplot(3,1,2)
yyaxis left
plot(t1,real(v1),'linewidth',1.5)
ylabel('v^1 (rad/s)')
hold on
yyaxis right
plot(t1,real(Fpto1),'linewidth',1.5)
ylabel('F_{PTO}^1 (Nm)')
xlabel('t^1 (sec)')

subplot(3,1,3)
plot(t1,Pm,'linewidth',1.5)
xlabel('t^1 (sec)')
ylabel('Mechanical Power')



