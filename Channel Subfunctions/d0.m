function y = d0(k,h,w)
y = k*sqrt(h+(1/w^2)*sinh(k*h)^2)/(sqrt(2)*w*cosh(k*h));
end