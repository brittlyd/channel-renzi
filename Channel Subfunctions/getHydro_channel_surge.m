function [HYDRO_params] = getHydro_channel_surge(OWSC_params,OWSC_params1,WAVE_params,WAVE_params1,COEFF_params)
%% Extract variables from params structure
%%% OWSC params
c = OWSC_params.c;
C = OWSC_params.C;
I = OWSC_params.I;
w = OWSC_params.w;

%%% OWSC params1
w1 = OWSC_params1.w1;

%%% Wave Params
h = WAVE_params.h;
omega = WAVE_params.omega;
Kappa_n = WAVE_params.Kappa_n;
k = WAVE_params.k;
A0 = WAVE_params.A0;
Cg = WAVE_params.Cg;

%%% Wave Params1
A01 = WAVE_params1.A01;
%omega1 = WAVE_params1.omega1;

%%% Coefficients
alpha = COEFF_params.alpha;
beta = COEFF_params.beta;


%% Calculate hydrodynamic parameters
%%% Added torque due to inertia
mu = (pi*w/4)*real(alpha(1,:)*fn(Kappa_n,h,c,omega).');

%%% Exciting torque
F = (-1i*omega*pi*w/4)*A0*fn(k,h,c,omega)*beta(1,1);

%%% Radiation damping
nu = (omega*w*pi/4)*imag(alpha(1,:)*fn(Kappa_n,h,c,omega).');

%%% Optimum PTO damping
nu_PTO_opt = sqrt((nu^2)+((((C-(I+mu)*omega^2))^2)/(omega^2)));
nu_PTO = nu_PTO_opt;

%%% Resonant C
C_res = (omega.^2).*(I+mu);

%%% Natural frequency and Critical Damping
omega_n = sqrt(C./(I+mu));
chi = (nu+nu_PTO)./(2*(I+mu).*omega_n);

%%% Theta magnitude
num = F;
den = ((-(omega.^2).*(I+mu))+C)-(1i*(omega.*(nu+nu_PTO)));
mag = abs(num)./abs(den); %(abs(F)./C)./(sqrt((((ones(size(omega)))-((omega./omega_n).^2)).^2)+(4*(chi.^2).*(omega./omega_n).^2)));

%%% Phase shift
phi = atan(imag(num)./real(num))-atan(imag(den)./real(den)); %(atan(imag(F)./real(F)))-(atan((-omega.*(nu+nu_PTO))./((-(omega.^2).*(I+mu))+C)));

%%% Theta and theta
Theta = mag.*exp(1i*phi);
Theta1 = (A01/w1).*Theta; %Theta*(Ai1/w1); %%%%%%%
%theta1 = real(Theta1.*exp(-1i.*omega1.*t1));
HYDRO_params.Theta1 = Theta1;
%HYDRO_params.theta1 = theta1;

%%% Angular velocity
V = 1i*omega*Theta;

%%% Capture Factor
CF = (abs(F).^2)./(2.*(A0.^2).*Cg.*(nu_PTO+nu));

%%% Power
Pa = (nu_PTO/2).*(omega^2).*(mag.^2);

%% Set Hydro params structure
HYDRO_params.mu = mu;
HYDRO_params.F = F;
HYDRO_params.nu = nu;
HYDRO_params.nu_PTO_opt = nu_PTO_opt;
HYDRO_params.nu_PTO = nu_PTO;
HYDRO_params.C_res = C_res;
HYDRO_params.omega_n = omega_n;
HYDRO_params.chi = chi;
HYDRO_params.mag = mag;
HYDRO_params.phi = phi;
HYDRO_params.Theta = Theta;
HYDRO_params.V = V;
HYDRO_params.CF = CF;
HYDRO_params.Pa = Pa;

end