function [HYDRO_params1] = dimensionalizeHydro(HYDRO_params,OWSC_params1,WAVE_params1)

eps = WAVE_params1.eps;
g = WAVE_params1.g;
rho = WAVE_params1.rho;
A1 = WAVE_params1.A1;
b1 = OWSC_params1.b1;


HYDRO_params1.mu1 = rho*b1^5*HYDRO_params.mu;
HYDRO_params1.F_mag1 = rho*g*A1*b1^3*abs(HYDRO_params.F);
HYDRO_params1.F1 = rho*g*A1*b1^3*HYDRO_params.F;
HYDRO_params1.nu1 = rho*b1^5*sqrt(g/b1)*HYDRO_params.nu;
HYDRO_params1.nu_PTO_opt1 = rho*b1^5*sqrt(g/b1)*HYDRO_params.nu_PTO_opt;
HYDRO_params1.theta1 = eps*HYDRO_params.theta;
HYDRO_params1.v1 = sqrt(g/b1)*eps*HYDRO_params.v;
HYDRO_params1.P1 = rho*A1^2*b1^(3/2)*g^(3/2)*HYDRO_params.P; 



end