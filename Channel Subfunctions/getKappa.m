function [WAVE_params_out] = getKappa(SIM_params,WAVE_params_in)
%% Extract variables from params structures
%%% WAVE params
h = WAVE_params_in.h;
omega = WAVE_params_in.omega;

%%% SIM params
n = SIM_params.n;
N = SIM_params.N;

%% Find wavenumbers
% kn_test = linspace(0,(N+1)*pi/h,1000);
% k_test = linspace(0,(N+1)*pi/h,1000);
f = @(k) omega^2+k.*tan(k.*h);
kn = zeros(size(n));

% for i = 1:5
%    kn(i) = fzero(f,(2*n(i)+1)*pi/(2*h)+0.01); 
% end

for ii = 1:N
   kn(ii) = fzero(f,(n(ii)+1)*pi/h); 
end

Kappa_n = kn*1i;

%%% Find initial wavenumber
const = (omega^2)*h;
k =  dispersion(const)/h;
Kappa_n(1) = k;

%%% Calculate group velocity
% Cg = (omega/(2*k))*(1+((2*k*h)/sinh(2*k*h)));

%     %%% Plot wavenumber solution for higher modes
%     figure(2)
%     plot(kn_test,-omega^2./kn_test,'k','linewidth',1.5)
%     hold on
%     plot(kn_test,tan(kn_test.*h),'b','linewidth',1.5)
%     plot(kn,-omega^2./kn,'ro--','linewidth',1.5)
%     ylim([-20,20])
%     xlim([0,(N+1)*pi/h])
%     legend('\omega^2/K_n','tan(K_nh)','K_n')
%     xlabel('K_n')
% 
%     %%% Plot wavenumber solution for first mode
%     figure(3)
%     plot(k_test,omega^2./k_test,'k','linewidth',1.5)
%     hold on
%     plot(k_test,tanh(k_test.*h),'b','linewidth',1.5)
%     plot(k,omega^2/k,'ro','linewidth',1.5)
%     legend('\omega^2/k','tanh(kh)','k')
%     ylim([0,4])
%     xlim([0,(N+1)*pi/h])
% 
%     %%% Plot vertical eigenmodes Zn(z)
%     figure(4)
%     z_test = linspace(-h,0,1000);
%     eig_modes = zeros(length(z_test),N);
%     for i = 1:N
%     eig_modes(:,i) = Zn(Kappa_n(i),z_test,h,omega);
%     plot(eig_modes(:,i),z_test,'linewidth',1.5)
%     hold on
%     end
%     plot(zeros(size(z_test)),z_test,'k--','linewidth',1.5)
%     plot(Zn(kn(1)*1i,z_test,h,omega),z_test,'k--','linewidth',1.5)
%     xlim([-5,5])
%     ylim([min(z_test),0])
%     ylabel('z')
%     % legend('n=0','n=1','n=2','n=3','n=4','location','SE')

%%% Set params structure
WAVE_params_out = WAVE_params_in;
WAVE_params_out.Kappa_n = Kappa_n;
WAVE_params_out.k = k;
% WAVE_params_out.Cg = Cg;
end