function y = sum3(M,us,v,k,w)
m = [-M:-1, 1:M];
f = @(m,u) besselh(1,0.5*k*w*abs(v-(2.*m./w)-u))./abs(v-(2.*m./w)-u);

sum1 = 0;
for ii = 1:length(m)
    sum1 = sum1+f(m(ii),us);
end

y = sum1;

end