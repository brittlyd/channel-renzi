function [TORQUES_params] = getTorques(WAVE_params,HYDRO_params,OWSC_params)
%% Extract variables from params structures
omega = WAVE_params.omega;

F = HYDRO_params.F;
Theta = HYDRO_params.Theta;
nu = HYDRO_params.nu;
mu = HYDRO_params.mu;
nu_PTO = HYDRO_params.nu_PTO;

C = OWSC_params.C;

%% Calculate Forces
Fe_coeff = F; %%% Excitation force
Fr_coeff = (omega^2*mu*Theta)+(1i*omega*nu*Theta); %%% Radiation force
Fpto_coeff = -1i*omega*nu_PTO*Theta;
Fb_coeff = C*Theta;

Fe_mag = abs(Fe_coeff);
Fr_mag = abs(Fr_coeff);
Fpto_mag = abs(Fpto_coeff);
Fb_mag = abs(Fb_coeff);

Fe_phi = imag(Fe_coeff)/real(Fe_coeff);
Fr_phi = imag(Fr_coeff)/real(Fr_coeff);
Fpto_phi = imag(Fpto_coeff)/real(Fpto_coeff);
Fb_phi = imag(Fb_coeff)/real(Fb_coeff);


%% Set Forces params structure
TORQUES_params.Fe_coeff = Fe_coeff;
TORQUES_params.Fr_coeff = Fr_coeff;
TORQUES_params.Fpto_coeff = Fpto_coeff;
TORQUES_params.Fb_coeff = Fb_coeff;

TORQUES_params.Fe_mag = Fe_mag;
TORQUES_params.Fr_mag = Fr_mag;
TORQUES_params.Fpto_mag = Fpto_mag;
TORQUES_params.Fb_mag = Fb_mag;

TORQUES_params.Fe_phi = Fe_phi;
TORQUES_params.Fr_phi = Fr_phi;
TORQUES_params.Fpto_phi = Fpto_phi;
TORQUES_params.Fb_phi = Fb_phi;



end