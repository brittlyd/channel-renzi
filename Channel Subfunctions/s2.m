function y = s2(x,J)
sum = 0;
for j = 2:J
   sum = sum+(((((-1).^(j+1)).*((x/2).^(2*j-1)))./(factorial(j)*...
       factorial(j-1))).*((1./j)+Hk(j)));
end
y = sum;

end