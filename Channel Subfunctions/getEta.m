function [ETA_params] = getEta(WAVE_params1,PHI_params1)
%% Extract parameters
%%% Wave parameters
omega1 = WAVE_params1.omega1;
g = WAVE_params1.g;

%%% Potentials
PHI1 = PHI_params1.PHI1;
PHI_I1 = PHI_params1.PHI_I1;
PHI_R1 = PHI_params1.PHI_R1;
PHI_D1 = PHI_params1.PHI_D1;

%% Calculate etas
eta_I = real((1i*omega1/g)*PHI_I1(1,:,:,:));
eta_R = real((1i*omega1/g)*PHI_R1(1,:,:,:));
eta_D = real((1i*omega1/g)*PHI_D1(1,:,:,:));
eta = real((1i*omega1/g)*PHI1(1,:,:,:));

%% Populate ETA structure
ETA_params.eta_I = eta_I;
ETA_params.eta_R = eta_R;
ETA_params.eta_D = eta_D;
ETA_params.eta = eta;

end