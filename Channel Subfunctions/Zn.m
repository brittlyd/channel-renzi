function y = Zn(kn,z,h,w)
y = sqrt(2).*cosh(kn.*(z+h))./sqrt(h+(1./w^2)*sinh(kn.*h).^2);
end