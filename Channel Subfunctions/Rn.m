function y = Rn(x,J)
gamma = 0.577215;
y = (besselj(1,x).*(1+((2*1i/pi).*(log(x./2)+gamma))))-((1i/pi)*((x./2)+s2(x,J)));
end