function [PRESSURE_params1] = getPressure(WAVE_params1,PHI_params1)
%% Extract parameters
%%% Wave parameters
omega1 = WAVE_params1.omega1;
rho = WAVE_params1.rho;

%%% Potentials
PHI1 = PHI_params1.PHI1;
PHI_I1 = PHI_params1.PHI_I1;
PHI_R1 = PHI_params1.PHI_R1;
PHI_D1 = PHI_params1.PHI_D1;

%% Calculate pressures
pressure_I1 = real((1i*omega1*rho)*PHI_I1);
pressure_R1 = real((1i*omega1*rho)*PHI_R1);
pressure_D1 = real((1i*omega1*rho)*PHI_D1);
pressure1 = real((1i*omega1*rho)*PHI1);

%% Populate PRESSURE structure
PRESSURE_params1.pressure_I1 = pressure_I1;
PRESSURE_params1.pressure_R1 = pressure_R1;
PRESSURE_params1.pressure_D1 = pressure_D1;
PRESSURE_params1.pressure1 = pressure1;

end