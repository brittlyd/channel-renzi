function y = Cpn_channel(v,k,w,p,J,u_test,M)
us = linspace(-1,1,u_test);

f = @(u) sqrt(1-u.^2).*chebyshevU(p,u).*((Rn(w.*k.*abs(v-u)./2,J)./abs(v-u))+sum3(M,u,v,k,w));

% figure
% plot(us,real(f(us)),'k','linewidth',1.5)
% hold on
% plot(us,imag(f(us)),'b','linewidth',1.5)
% legend('real','imaginary')
% title(strcat('p=',num2str(p),', j=',num2str(jj),', n=',num2str(nn),', v0=',num2str(v)))

y = (-pi*(p+1)*chebyshevU(p,v))+((1i*pi*k*w/4)*trapz(us,f(us)));
end