function [COEFF_params] = getAlphaBeta_channel(SIM_params,OWSC_params,WAVE_params)
%% Get variables from params structures
%%% Simulation params
P = SIM_params.P;
N = SIM_params.N;
u_test = SIM_params.u_num;
M = SIM_params.M;
v0j = SIM_params.v0j;
J = SIM_params.J;

%%% OWSC params
c = OWSC_params.c;
w = OWSC_params.w;

%%% Wave params
k = WAVE_params.k;
h = WAVE_params.h;
omega = WAVE_params.omega;
Kappa_n = WAVE_params.Kappa_n;


%% Calculate complex amplitudes alpha and beta
b = zeros(P+1,N);
alpha = zeros(P+1,N);
A = zeros(P+1,P+1,N);

for nn = 1:N % modes
    for pp=1:P+1 
       for jj=1:P+1
          A(jj,pp,nn) = Cpn_channel(v0j(jj),Kappa_n(nn),w,pp-1,J,u_test,M); 
       end
    end
    b(:,nn) = -pi*w*fn(Kappa_n(nn),h,c,omega)*ones(P+1,1);
    alpha(:,nn) = A(:,:,nn)\b(:,nn);
end

beta = zeros(size(alpha));
beta(:,1) = alpha(:,1).*d0(k,h,omega)./fn(k,h,c,omega);

%%%% Set Hydro params structure
COEFF_params.alpha = alpha;
COEFF_params.beta = beta;

end
