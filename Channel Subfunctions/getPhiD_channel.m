function [Phi_D] = getPhiD_channel(SIM_params,WAVE_params,COEFF_params,OWSC_params,POS_params)
%%% Simulation params
P = SIM_params.P;
% N = SIM_params.N;
u_num = SIM_params.u_num;
M = SIM_params.M;

%%% OSWEC params
w = OWSC_params.w;

%%% Wave params
omega = WAVE_params.omega;
h = WAVE_params.h;
k = WAVE_params.k;
A0 = WAVE_params.A0;

%%% Coefficients
% alpha = COEFF_params.alpha;
beta = COEFF_params.beta;

%%% Position params
x = POS_params.x;
y = POS_params.y;
z = POS_params.z;

if abs(x)<10^-10
   Phi_D = 0;
   return
end

f = @(u,p,m) sqrt(1-u.^2).*chebyshevU(2*p,u).*besselh(1,k*sqrt((x.^2)+((y-(w*u/2)-m).^2)))./sqrt((x.^2)+((y-(w*u/2)-m).^2));
u_span = linspace(-1,1,u_num);
m = [-M:-1 1:M];
    
sum1 = 0;
for pp = 0:(P/2)
    
    sum_m = 0;
    for mm = 1:length(m)
       sum_m = sum_m+trapz(u_span,f(u_span,pp,m(mm))); 
    end
    
    sum1 = sum1+(beta((2*pp)+1,1).*sum_m);
end
    

Phi_D = (-1i*w*A0/8).*k.*x.*Zn(k,z,h,omega).*sum1;
end