function [TORQUES_params1] = dimensionalizeTorques(TORQUES_params,OWSC_params1,WAVE_params1,XT_params1)
g = WAVE_params1.g;
rho = WAVE_params1.rho;
A1 = WAVE_params1.A1;
omega1 = WAVE_params1.omega1;
b1 = OWSC_params1.b1;
t1 = XT_params1.t1;

%%% Dimensionalize Force magnitude
TORQUES_params1.Fe_mag1 = (rho*g*A1*b1^3)*TORQUES_params.Fe_mag;
TORQUES_params1.Fr_mag1 = (rho*g*A1*b1^3)*TORQUES_params.Fr_mag;
TORQUES_params1.Fpto_mag1 = (rho*g*A1*b1^3)*TORQUES_params.Fpto_mag;
TORQUES_params1.Fb_mag1 = (rho*g*A1*b1^3)*TORQUES_params.Fb_mag;

%%% Get dimensional time series
TORQUES_params1.Fe1 = TORQUES_params1.Fe_mag1*exp(1i*TORQUES_params.Fe_phi)*exp(-1i*omega1*t1);
TORQUES_params1.Fr1 = TORQUES_params1.Fr_mag1*exp(1i*TORQUES_params.Fr_phi)*exp(-1i*omega1*t1);
TORQUES_params1.Ft1 = TORQUES_params1.Fe1+TORQUES_params1.Fr1;
TORQUES_params1.Fpto1 = TORQUES_params1.Fpto_mag1*exp(1i*TORQUES_params.Fpto_phi)*exp(-1i*omega1*t1);
TORQUES_params1.Fb1 = TORQUES_params1.Fb_mag1*exp(1i*TORQUES_params.Fb_phi)*exp(-1i*omega1*t1);

end