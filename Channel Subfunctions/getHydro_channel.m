function [HYDRO_params] = getHydro_channel(OWSC_params,WAVE_params,COEFF_params)
%% Extract variables from params structure
%%% OWSC params
c = OWSC_params.c;
C = OWSC_params.C;
I = OWSC_params.I;
w = OWSC_params.w;

%%% Wave Params
h = WAVE_params.h;
omega = WAVE_params.omega;
Kappa_n = WAVE_params.Kappa_n;
k = WAVE_params.k;
A0 = WAVE_params.A0;

%%% Coefficients
alpha = COEFF_params.alpha;
beta = COEFF_params.beta;


%% Calculate hydrodynamic parameters
%%% Added torque due to inertia
mu = (pi*w/4)*real(alpha(1,:)*fn(Kappa_n,h,c,omega).');

%%% Exciting torque
F = (-1i*omega*pi*w/4)*A0*fn(k,h,c,omega)*beta(1,1);

%%% Radiation damping
nu = (omega*w*pi/4)*imag(alpha(1,:)*fn(Kappa_n,h,c,omega).');

%%% Optimum PTO damping
nu_PTO_opt = sqrt((nu^2)+((((C-(I+mu)*omega^2))^2)/(omega^2)));

%% Set Hydro params structure
HYDRO_params.mu = mu;
HYDRO_params.F = F;
HYDRO_params.nu = nu;
HYDRO_params.nu_PTO_opt = nu_PTO_opt;

end