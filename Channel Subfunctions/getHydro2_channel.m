function [HYDRO_params] = getHydro2_channel(HYDRO_params,OWSC_params,...
    WAVE_params,XT_params)
%% Extract variables from params structure
%%% OWSC params
C = OWSC_params.C;
I = OWSC_params.I;

%%% Wave Params
omega = WAVE_params.omega;

%%% Hydro
mu = HYDRO_params.mu;
F = HYDRO_params.F;
nu = HYDRO_params.nu;
nu_PTO = HYDRO_params.nu_PTO;
C_PTO = HYDRO_params.C_PTO;
mu_PTO = HYDRO_params.mu_PTO;

%%% Time
t = XT_params.t;

%% Calculate hydrodynamic parameters
%%% Resonant C
% C_res = (omega.^2).*(I+mu);

%%% Natural frequency and Critical Damping
omega_n = sqrt((C+C_PTO)./(I+mu+mu_PTO));
% chi = (nu+nu_PTO)./(2*(I+mu).*omega_n);

%%% Theta magnitude
num = F;
den = ((-(omega.^2).*(I+mu+mu_PTO))+C+C_PTO)-(1i*omega.*(nu+nu_PTO));
mag = abs(num)./abs(den); %(abs(F)./C)./(sqrt((((ones(size(omega)))-((omega./omega_n).^2)).^2)+(4*(chi.^2).*(omega./omega_n).^2)));

%%% Phase shift
phi = atan(imag(num)./real(num))-atan(imag(den)./real(den)); %(atan(imag(F)./real(F)))-(atan((-omega.*(nu+nu_PTO))./((-(omega.^2).*(I+mu))+C)));

%%% Theta and theta
Theta = mag.*exp(1i*phi);
theta = Theta.*exp(-1i.*omega.*t);


%%% Angular velocity
V = 1i*omega*Theta;
v = V.*exp(-1i.*omega.*t);


%%% Power
P = (nu_PTO/2).*(omega^2).*(mag.^2);


%% Set Hydro params structure
% HYDRO_params.C_res = C_res;
HYDRO_params.omega_n = omega_n;
% HYDRO_params.chi = chi;
HYDRO_params.mag = mag;
HYDRO_params.phi = phi;
HYDRO_params.Theta = Theta;
HYDRO_params.theta = theta;
HYDRO_params.V = V;
HYDRO_params.v = v;
HYDRO_params.P = P;

end