function [FORCES_params] = getSurgeForce(XT_params,SIM_params,OWSC_params,...
    WAVE_params,COEFF_params,HYDRO_params)
    x = XT_params.x;
    y = XT_params.y;
    z = XT_params.z;
    t = XT_params.t;
    N = SIM_params.N;
    P = SIM_params.P;
    delta_phir  = zeros(length(y),N);
    delta_phid = zeros(length(y),N);
    delta_phi = zeros(length(y),N);
    Zn_vec = zeros(length(z),N);
    int_yn = zeros(1,N);
    int_zn = zeros(1,N);
    for nn = 1:N
        for yy = 1:length(y)
            u = 2*y(yy)/OWSC_params.w;
            sum_r = 0;
            sum_d = 0;
            for pp = 1:P
                sum_r = sum_r+(COEFF_params.alpha(pp,nn)*chebyshevU(pp-1,u));
                sum_d = sum_d+(COEFF_params.beta(pp,nn)*chebyshevU(pp-1,u));
            end
            delta_phir(yy,nn) = HYDRO_params.V.*sqrt((1-(u.^2))).*sum_r;
            delta_phid(yy,nn) = WAVE_params.A0.*sqrt((1-(u.^2))).*sum_d;

        end
        Zn_vec(:,nn) = Zn(WAVE_params.Kappa_n(nn),z,WAVE_params.h,OWSC_params.w);
        delta_phi(:,nn) = delta_phir(:,nn)+delta_phid(:,nn);
        
        int_yn(nn) = trapz(y,delta_phi(:,nn));
        int_zn(nn) = trapz(z,Zn_vec(:,nn));
    end
    
    omega = WAVE_params.omega;
    
    F_surge_coeff = 1i*omega*sum(int_yn.*int_zn);
    F_surge_mag = abs(F_surge_coeff);
    F_surge_phi = imag(F_surge_coeff)/real(F_surge_coeff);
    F_surge = F_surge_mag*exp(1i*F_surge_phi)*exp(-1i*omega*t);
    
    FORCES_params.F_surge = F_surge;
    FORCES_params.F_surge_mag = F_surge_mag;
    FORCES_params.F_surge_coeff = F_surge_coeff;
    FORCES_params.F_surge_phi = F_surge_phi;


end