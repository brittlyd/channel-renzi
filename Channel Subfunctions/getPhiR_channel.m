function [Phi_R] = getPhiR_channel(SIM_params,WAVE_params,COEFF_params,HYDRO_params,OWSC_params,POS_params)
%%% Simulation params
P = SIM_params.P;
N = SIM_params.N;
u_num = SIM_params.u_num;
M = SIM_params.M;

%%% OSWEC params
w = OWSC_params.w;

%%% Wave params
omega = WAVE_params.omega;
h = WAVE_params.h;
Kappa_n = WAVE_params.Kappa_n;

%%% Coefficients
alpha = COEFF_params.alpha;
% beta = COEFF_params.beta;

%%% Hydro params
V = HYDRO_params.V;

%%% Position params
x = POS_params.x;
y = POS_params.y;
z = POS_params.z;

if abs(x)<10^-10
   Phi_R = 0;
   return
end

f = @(u,p,n,m) sqrt(1-u.^2).*chebyshevU(2*p,u).*besselh(1,Kappa_n(n)*sqrt((x.^2)+((y-(w*u/2)-m).^2)))./sqrt((x.^2)+((y-(w*u/2)-m).^2));
u_span = linspace(-1,1,u_num);
m = [-M:-1 1:M];

sum2 = 0;
for nn = 1:N
    
    sum1 = 0;
    for pp = 0:(P/2)
        
        sum_m = 0;
        for mm = 1:length(m)
           sum_m = sum_m+trapz(u_span,f(u_span,pp,nn,m(mm))); 
        end
        
        sum1 = sum1+(alpha((2*pp)+1,nn).*sum_m);
    end
    
    sum2 = sum2+(Kappa_n(nn).*x.*Zn(Kappa_n(nn),z,h,omega).*sum1);

end

Phi_R = (-1i*V/8).*sum2;
return
end

