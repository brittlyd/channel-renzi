function y = fn(kn,h,c,w)
y = sqrt(2)*((kn.*(h-c).*sinh(kn*h))+cosh(kn.*c)-cosh(kn.*h))./(kn.^2.*...
    sqrt(h+((1./w.^2).*(sinh(kn.*h).^2))));
end