function [Phi_I] = getPhiI_channel(WAVE_params,POS_params)
A0 = WAVE_params.A0;
omega = WAVE_params.omega;
k = WAVE_params.k;
h = WAVE_params.h;

x = POS_params.x;
% y = POS_params.y;
z = POS_params.z;

Phi_I = (-1i.*A0./omega).*(cosh(k.*(z+h))./cosh(k.*h)).*exp(-1i.*k.*x);
end