function [PHI_params] = getPHI_channel(SIM_params,OWSC_params,...
    WAVE_params,COEFF_params,HYDRO_params,POINT_params)
%% Get spatial potentials, nondimensional
Phi_I = getPhiI_channel(WAVE_params,POINT_params);
Phi_R = getPhiR_channel(SIM_params,WAVE_params,COEFF_params,HYDRO_params,...
    OWSC_params,POINT_params);
Phi_D = getPhiD_channel(SIM_params,WAVE_params,COEFF_params,OWSC_params,POINT_params);

% Save in nondimestional structure
PHI_params.Phi_I = Phi_I;
PHI_params.Phi_R = Phi_R;
PHI_params.Phi_D = Phi_D;


%% Get time-dependent potentials, nondimensional
PHI = (Phi_I+Phi_R+Phi_D).*exp(-1i.*WAVE_params.omega.*POINT_params.t);
PHI_I = Phi_I.*exp(-1i.*WAVE_params.omega.*POINT_params.t);
PHI_R = Phi_R.*exp(-1i.*WAVE_params.omega.*POINT_params.t);
PHI_D = Phi_D.*exp(-1i.*WAVE_params.omega.*POINT_params.t);

% Save in nondimensional structure
PHI_params.PHI = PHI;
PHI_params.PHI_I = PHI_I;
PHI_params.PHI_R = PHI_R;
PHI_params.PHI_D = PHI_D;

end