function [OWSC_params, WAVE_params, XT_params] = nondimensionalize_channel(OWSC_params1, WAVE_params1, XT_params1)
%%%% OWSC params
w1 = OWSC_params1.w1;
c1 = OWSC_params1.c1;
C1 = OWSC_params1.C1;
I1 = OWSC_params1.I1;
b1 = OWSC_params1.b1;

%%%% Wave params
g = WAVE_params1.g;
omega1 = WAVE_params1.omega1;
rho = WAVE_params1.rho;
h1 = WAVE_params1.h1;
A01 = WAVE_params1.A01;
A1 = WAVE_params1.A1;
T1 = WAVE_params1.T1;

%%% Time and space parameters
x1 = XT_params1.x1;
y1 = XT_params1.y1;
z1 = XT_params1.z1;
t1 = XT_params1.t1;

%% Nondimensionalize parameters
%%%% OWSC parameters
w = w1/b1;
c = c1/b1;
C = C1/(rho*b1^4);
I = I1/(rho*b1^5);

%%% Set output structure
OWSC_params.w = w;
OWSC_params.c = c;
OWSC_params.C = C;
OWSC_params.I = I;

%%%% Wave parameters
T = sqrt(g/b1)*T1;
omega = sqrt(b1/g)*omega1;
h = h1/b1;
A0 = A01/A1; % Nondimensional wave height

%%% Set output structure
WAVE_params.T = T;
WAVE_params.omega = omega;
WAVE_params.h = h;
WAVE_params.A0 = A0;

%%% Time and space parameters
x = x1/b1;
y = y1/b1;
z = z1/b1;
t = sqrt(g/b1)*t1;

%%% Set output structure
XT_params.x = x;
XT_params.y = y;
XT_params.z = z;
XT_params.t = t;

end