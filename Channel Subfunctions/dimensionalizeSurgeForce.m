function [FORCES_params1] = dimensionalizeSurgeForce(FORCES_params,...
    WAVE_params1,OWSC_params1,XT_params1)

rho = WAVE_params1.rho;
g = WAVE_params1.g;
A1 = WAVE_params1.A1;
b1 = OWSC_params1.b1;
omega1 = WAVE_params1.omega1;
t1 = XT_params1.t1;

FORCES_params1.F1_surge_mag = (rho*g*A1*b1^3)*FORCES_params.F_surge_mag;
FORCES_params1.F1_surge = FORCES_params1.F1_surge_mag*...
    exp(1i*FORCES_params.F_surge_phi)*exp(-1i*omega1*t1);

end