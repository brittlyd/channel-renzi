clear; close all; clc;
cd('C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi');
filename_prefix = 'channel_data_surge';
mkdir(filename_prefix);
folder = strcat('C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi\',filename_prefix);
cd(folder)

T_test = 0.5:0.25:3;
% mu1 = zeros(size(T_test));
% F1_mag = zeros(size(T_test));
% Theta_mag1 = zeros(size(T_test));
F_surge_vec = zeros(length(T_test),1);
F_surge_mag_vec = zeros(length(T_test),1);
F1_surge_mag_vec = zeros(length(T_test),1);
tic
for tt = 1:length(T_test)
    clearvars -except T_test filename_prefix tt ...
        F_surge_vec F_surge_mag_vec F1_surge_mag_vec 
    clf
    close all
    filename = strcat(filename_prefix,num2str(tt));


    %% Define parameters of sums and solving process
    P = 4; % Upper limit of sums
    N = 4; % Number of modes considered
    M = 10;
    u_num = 100;
    j = 0:P; 
    n = 0:N-1; % Vector of modes
    v0j = cos(((2*j+1)*pi)/((2*P)+2)); % Calculate evaluation points
    J = 100; % Upper limit for sub-series

    %%%% Set simulation params structure
    SIM_params.P = P;
    SIM_params.N = N;
    SIM_params.u_num = u_num;
    SIM_params.M = M;
    SIM_params.n = n;
    SIM_params.v0j = v0j;
    SIM_params.J = J;

    %% Dimenionsional parameters
    g = 9.81;
    %%% Define OSWEC parameters (full-scale, dimensional)
    a1 = 0.1;
    w1 = 0.85; % Flap width
    c1 = 0.2; % Height of foundation
    m = 16; % Mass of flap
    m_w = 45; % Mass of displaced water (fully submerged)
    V = 0.04584; % Volume of flap
    Fg = m*g;
    Fb = m_w*g;
    db = 0.230; % Distance along flap to center of buoyancy
    dg = 0.230; % Distance along flap to center of mass
    C1 = (Fb*db-Fg*dg); 
    I1 = 1.25;
    b1 = 0.9;


    %%%% Set OWSC params1 structure
    OWSC_params1.w1 = w1;
    OWSC_params1.c1 = c1;
    OWSC_params1.C1 = C1;
    OWSC_params1.I1 = I1;
    OWSC_params1.b1 = b1;

    %%% Define wave and ocean paramenters (full-scale, dimentional) and fill
    %%% WAVE_params structure
    rho = 1000;
    h1 = 0.7; % Depth of water

    A01 = 0.1; % Incident wave height [m] (between 5 and 15 cm)
    A1 = a1; %w1*0.05; % I don't actually know what this is? Amplitude scale, must be much smaller than w1

    T1 = T_test(tt); %4:14; % Period of wave
    omega1 = 2*pi/T1; % Frequency of wave

    %%%% Set WAVE params1 structure
    WAVE_params1.g = g;
    WAVE_params1.rho = rho;
    WAVE_params1.h1 = h1;
    WAVE_params1.A01 = A01;
    WAVE_params1.A1 = A1;
    WAVE_params1.T1 = T1;
    WAVE_params1.omega1 = omega1;

    eps = A01/b1;

    %% Define time and space
    % xnum = 50;
    ynum = 50;
    znum = 50;
    % tnum = 100;
    % 
    %%% Dimensional time and space
    % x1 = linspace(-4,4,xnum);
    y1 = linspace(-0.5*b1,0.5*b1,ynum);
    z1 = linspace(-h1,0,znum);
    % t1 = linspace(0,5*T1,tnum);
    % 
    %%% Nondimensional time and space
    % x = x1/b1;
    y = y1/b1;
    z = z1/b1;
    % t = t1/b1;

    %% Nondimensionalize
    [OWSC_params,WAVE_params] = nondimensionalize_channel(OWSC_params1,WAVE_params1);

    %% Get wavenumbers
    [WAVE_params] = getKappa(SIM_params,WAVE_params);

    %% Get Alpha and Beta
    [COEFF_params] = getAlphaBeta_channel(SIM_params,OWSC_params,WAVE_params);

    %% Get Hydro params
    [HYDRO_params] = getHydro_channel_surge(OWSC_params,OWSC_params1,WAVE_params,WAVE_params1,COEFF_params);
    
    %% Calculate surge forces
    delta_phir  = zeros(length(y),N);
    delta_phid = zeros(length(y),N);
    delta_phi = zeros(length(y),N);
    Zn_vec = zeros(length(z),N);
    int_yn = zeros(1,N);
    int_zn = zeros(1,N);
    colors = 'rbgmc';
    for nn = 1:N
        for yy = 1:length(y)
            u = 2*y(yy)/OWSC_params.w;
            sum_r = 0;
            sum_d = 0;
            for pp = 1:P
                sum_r = sum_r+(COEFF_params.alpha(pp,nn)*chebyshevU(pp-1,u));
                sum_d = sum_d+(COEFF_params.beta(pp,nn)*chebyshevU(pp-1,u));
            end
            delta_phir(yy,nn) = HYDRO_params.V.*sqrt((1-(u.^2))).*sum_r;
            delta_phid(yy,nn) = WAVE_params.A0.*sqrt((1-(u.^2))).*sum_d;

        end
        Zn_vec(:,nn) = Zn(WAVE_params.Kappa_n(nn),z,WAVE_params.h,OWSC_params.w);
        delta_phi(:,nn) = delta_phir(:,nn)+delta_phid(:,nn);

        hh1 = figure(1);
        subplot(3,1,1)
        plot(y,abs(delta_phir(:,nn)),colors(nn),'linewidth',1.5)
        hold on
        vline(-0.5*OWSC_params.w,'k-')
        vline(0.5*OWSC_params.w,'k-')
        xlabel('y')
        ylabel('\Delta \phi_{r,n}')
        
        %hh2 = figure(2);
        subplot(3,1,2)
        plot(y,abs(delta_phid(:,nn)),colors(nn),'linewidth',1.5)
        hold on
        vline(-0.5*OWSC_params.w,'k-')
        vline(0.5*OWSC_params.w,'k-')
        xlabel('y')
        ylabel('\Delta \phi_{d,n}')
        
        %hh3 = figure(3);
        subplot(3,1,3)
        plot(y,abs(delta_phi(:,nn)),colors(nn),'linewidth',1.5)
        hold on
        vline(-0.5*OWSC_params.w,'k-')
        vline(0.5*OWSC_params.w,'k-')
        xlabel('y')
        ylabel('\Delta \phi_n')
        legend('n=1','n=2','n=3','n=4')

        hh4 = figure(4);
        plot(z,abs(Zn_vec(:,nn)),colors(nn),'linewidth',1.5)
        hold on
        xlabel('z')
        ylabel('Z_n')

        int_yn(nn) = trapz(y,delta_phi(:,nn));
        int_zn(nn) = trapz(z,Zn_vec(:,nn));
    end
    omega = WAVE_params.omega;
    F_surge = 1i*omega*sum(int_yn.*int_zn);
    F_surge_mag = abs(F_surge);

    F1_surge_mag = (rho*g*A1*b1^3)*F_surge_mag;
    
    save(filename)
    
    F_surge_vec(tt) = F_surge;
    F_surge_mag_vec(tt) = F_surge_mag;
    F1_surge_mag_vec(tt) = F1_surge_mag;
    
    tt
    toc
end

hh5 = figure(5)
plot(T_test,F1_surge_mag_vec,'ko','linewidth',1.5)
xlabel('T^1 [sec]')
ylabel('F^1_{surge} [N]')
saveas(gcf,strcat(filename_prefix,'_surge.png'))

surge_filename = 'channel_WECSIM_surge_data_forces';
save(surge_filename,'F_surge_vec','F_surge_mag_vec','F1_surge_mag_vec')




