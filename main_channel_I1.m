clear; close all; clc;

filename = 'channel_I1.mat';

%% Define parameters of sums and solving process
P = 4; % Upper limit of sums
N = 4; % Number of modes considered
M = 10;
u_num = 100;
j = 0:P; 
n = 0:N-1; % Vector of modes
v0j = cos(((2*j+1)*pi)/((2*P)+2)); % Calculate evaluation points
J = 100; % Upper limit for sub-series

%%%% Set simulation params structure
SIM_params.P = P;
SIM_params.N = N;
SIM_params.u_num = u_num;
SIM_params.M = M;
SIM_params.n = n;
SIM_params.v0j = v0j;
SIM_params.J = J;
    
I1_test = [0.1:1:10.1];

mu = zeros(1,length(I1_test));
F = zeros(1,length(I1_test));
nu = zeros(1,length(I1_test));
Theta1 = zeros(1,length(I1_test));
Theta1_mag = zeros(1,length(I1_test));
Theta1_phase = zeros(1,length(I1_test));
theta1 = zeros(100,length(I1_test));
CF = zeros(1,length(I1_test));

tic
for cc = 1:length(I1_test)
    %% Dimenionsional parameters
    %%% Define OSWEC parameters (full-scale, dimensional)
    w1 = 0.8; % Flap width
    c1 = 0.1; % Height of foundation
    C1 = 0.5; 
    I1 = I1_test(cc);
    b1 = 0.9;

    %%%% Set OWSC params1 structure
    OWSC_params1.w1 = w1;
    OWSC_params1.c1 = c1;
    OWSC_params1.C1 = C1;
    OWSC_params1.I1 = I1;
    OWSC_params1.b1 = b1;

    %%% Define wave and ocean paramenters (full-scale, dimentional) and fill
    %%% WAVE_params structure
    g = 9.81;
    rho = 1000;
    h1 = 0.76; % Depth of water

    A01 = 0.1; % Incident wave height [m] (between 5 and 15 cm)
    A1 = b1/10; % I don't actually know what this is? Amplitude scale, must be much smaller than w1

    T1 = 1.5; %4:14; % Period of wave
    omega1 = 2*pi/T1; % Frequency of wave

    %%%% Set WAVE params1 structure
    WAVE_params1.g = g;
    WAVE_params1.rho = rho;
    WAVE_params1.h1 = h1;
    WAVE_params1.A01 = A01;
    WAVE_params1.A1 = A1;
    WAVE_params1.T1 = T1;
    WAVE_params1.omega1 = omega1;

    eps = A01/b1;

    %% Define time and space
    xnum = 50;
    ynum = 10;
    znum = 10;
    tnum = 100;

    %%% Dimensional time and space
    x1 = linspace(-4,4,xnum);
    y1 = linspace(-0.5*b1,0.5*b1,ynum);
    z1 = linspace(-h1,0,znum);
    t1 = linspace(0,5*T1,tnum);

    %%% Nondimensional time and space
    x = x1/b1;
    y = y1/b1;
    z = z1/b1;
    t = t1/b1;

    %% Nondimensionalize parameters
    [OWSC_params,WAVE_params] = nondimensionalize_channel(OWSC_params1,WAVE_params1);

    %% Get wavenumbers
    [WAVE_params] = getKappa(SIM_params,WAVE_params);

    %% Get Alpha and Beta
    [COEFF_params] = getAlphaBeta_channel(SIM_params,OWSC_params,WAVE_params);

    %% Get Hydro params (1)
    [HYDRO_params] = getHydro_channel(OWSC_params,WAVE_params,COEFF_params);
    mu(cc) = HYDRO_params.mu;
    F(cc) = HYDRO_params.F;
    nu(cc) = HYDRO_params.nu;

    %% PTO
    nu_PTO = HYDRO_params.nu_PTO_opt;
    HYDRO_params.nu_PTO = nu_PTO;

    %% Get Hydro params (2)
    [HYDRO_params] = getHydro2_channel(HYDRO_params,OWSC_params,WAVE_params,OWSC_params1,WAVE_params1,t1);
    Theta1(cc) = HYDRO_params.Theta1;
    Theta1_mag(cc) = abs(Theta1(cc));
    Theta1_phase(cc) = imag(Theta1(cc))/real(Theta1(cc));
    theta1(:,cc) = HYDRO_params.theta1;
    CF(cc) = HYDRO_params.CF;
    
    toc
    cc
end
% mu1 = (rho*b1^5)*mu;
% F1 = (rho*g*A1*b1^3)*abs(F);
% nu1 = 

h1 = figure(1);
plot(I1_test,(180/pi)*Theta1_mag,'k','linewidth',1.5)
title('|\Theta|')
xlabel('I1 [kg m^2]')
ylabel('Degrees')

h2 = figure(2);
plot(I1_test,(180/pi)*Theta1_phase,'k','linewidth',1.5)
title('\Theta Phase')
xlabel('I1 [kg m^2]')
ylabel('Degrees')

h3 = figure(3);
plot(I1_test,CF,'k','linewidth',1.5)
title('Capture Factor')
xlabel('I1 [kg m^2]')

save(filename)

