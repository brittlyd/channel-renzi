clear; close all; clc;
main_dir = 'C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi';
filename_prefix = 'channel_h1';
folder = strcat(main_dir,'\',filename_prefix);
cd(folder)
load(strcat(filename_prefix,'.mat'));

for i = 1:length(h1_vec)
    Fpto_mag1(i) = TORQUES_params1(i).Fpto_mag1;
end

figure(1)
plot(h1_vec,Fpto_mag1,'o-','linewidth',1.5)
xlabel('h^1 (m)')
ylabel('PTO Torque Magnitude (Nm)')
