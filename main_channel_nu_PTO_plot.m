clear; close all; clc;
filename_prefix = 'channel_data_torques_c1_015_opt';
folder = strcat('C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi\',filename_prefix);
cd(folder)
nu_num = 1;
%%
for nn = 1:nu_num
    load(strcat(filename_prefix,'_',num2str(nn)))
    
    Fe1 = real(TORQUES_params1.Fe1);
    Fr1 = real(TORQUES_params1.Fr1);
    Ft1 = real(TORQUES_params1.Ft1);
    Fb1 = real(TORQUES_params1.Fb1);
    Fpto1 = real(TORQUES_params1.Fpto1);
    
    figure
    plot(t1,Fe1,'b','linewidth',1.5)
    hold on
    plot(t1,Fr1,'r','linewidth',1.5)
    % plot(t1,Ft1,'k','linewidth',1.5)
    plot(t1,Fb1,'g','linewidth',1.5)
    plot(t1,Fpto1,'k','linewidth',1.5)
    xlabel('t [s]')
    ylabel('[Nm]')
    legend('F_e','F_r','F_b','F_{PTO}','location','SE')
    title('Torque Comparison')
%     title(strcat('\nu_{PTO}=',num2str(nu_PTO)))
    saveas(gcf,strcat(filename_prefix,'_',num2str(nn),'_Torques.png'))
    
    Theta1 = HYDRO_params.Theta1;
    v1 = HYDRO_params.v1;

    figure
    yyaxis left
    plot(t1,real(v1),'linewidth',1.5)
    ylabel('V^1 (rad/s)')
    ylim([-1 1]*1.5)
    hold on
    yyaxis right
    plot(t1,real(Fpto1),'-','linewidth',1.5)
    ylabel('F_{PTO}^1 [N]')
    xlabel('t^1 [s]')
    ylim([-1 1]*300)
%     title(strcat('\nu_{PTO}=',num2str(nu_PTO)))
    saveas(gcf,strcat(filename_prefix,'_',num2str(nn),'_V_F_PTO.png'))
    
end