clear; close all; clc;
cd('C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi');
filename_prefix = 'channel_data_torques_c1_015';
mkdir(filename_prefix);
folder = strcat('C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi\',filename_prefix);
cd(folder)

pdf_struct = struct();

%% Define parameters of sums and solving process
P = 4; % Upper limit of sums
N = 4; % Number of modes considered
M = 10;
u_num = 100;
j = 0:P; 
n = 0:N-1; % Vector of modes
v0j = cos(((2*j+1)*pi)/((2*P)+2)); % Calculate evaluation points
J = 100; % Upper limit for sub-series

%%%% Set simulation params structure
SIM_params.P = P;
SIM_params.N = N;
SIM_params.u_num = u_num;
SIM_params.M = M;
SIM_params.n = n;
SIM_params.v0j = v0j;
SIM_params.J = J;

%% Dimenionsional parameters
g = 9.81;
%%% Define OSWEC parameters (full-scale, dimensional)
w1 = 0.85; % Flap width
c1 = 0.1; % Height of foundation
m = 16; % Mass of flap
m_w = 45; % Mass of displaced water (fully submerged)
Vol = 0.04584; % Volume of flap
Fg = m*g;
Fb = m_w*g;
db = 0.230; % Distance along flap to center of buoyancy
dg = 0.230; % Distance along flap to center of mass
C1 = (Fb*db-Fg*dg); 
I1 = 1.25;
b1 = 0.9;

%%%% Set OWSC params1 structure
OWSC_params1.w1 = w1;
OWSC_params1.c1 = c1;
OWSC_params1.C1 = C1;
OWSC_params1.I1 = I1;
OWSC_params1.b1 = b1;

%%% Define wave and ocean paramenters (full-scale, dimentional) and fill
%%% WAVE_params structure
rho = 1000;
h1 = 0.7; % Depth of water

A01 = 0.1; % Incident wave height [m] (between 5 and 15 cm)
A1 = b1/10; % I don't actually know what this is? Amplitude scale, must be much smaller than w1

T1 = 1.5; %4:14; % Period of wave
omega1 = 2*pi/T1; % Frequency of wave

%%%% Set WAVE params1 structure
WAVE_params1.g = g;
WAVE_params1.rho = rho;
WAVE_params1.h1 = h1;
WAVE_params1.A01 = A01;
WAVE_params1.A1 = A1;
WAVE_params1.T1 = T1;
WAVE_params1.omega1 = omega1;

eps = A01/b1;

%% Define time and space
% xnum = 50;
% ynum = 20;
% znum = 20;
tnum = 100;

%%% Dimensional time and space
% x1 = linspace(-2,2,xnum);
% y1 = linspace(-0.5*b1,0.5*b1,ynum);
% z1 = linspace(-h1,0,znum);
t1 = linspace(0,T1,tnum);

%%% Nondimensional time and space
% x = x1/b1;
% y = y1/b1;
% z = z1/b1;
t = t1/b1;

%% Nondimensionalize parameters
[OWSC_params,WAVE_params] = nondimensionalize_channel(OWSC_params1,WAVE_params1);

%% Get wavenumbers
[WAVE_params] = getKappa(SIM_params,WAVE_params);

%% Get Alpha and Beta
[COEFF_params] = getAlphaBeta_channel(SIM_params,OWSC_params,WAVE_params);

%% Get Hydro params (1)
[HYDRO_params] = getHydro_channel(OWSC_params,WAVE_params,COEFF_params);


nu_PTO_test = 0:0.01:0.2; %HYDRO_params.nu_PTO_opt;
pdf_struct.nu_PTO = nu_PTO_test;
tic
for nn = 1:length(nu_PTO_test)
    %% PTO
    nu_PTO = nu_PTO_test(nn);
    HYDRO_params.nu_PTO = nu_PTO;

    %% Get Hydro params (2)
    [HYDRO_params] = getHydro2_channel(HYDRO_params,OWSC_params,WAVE_params,OWSC_params1,WAVE_params1,t1);

    %% Calculate torques
    [TORQUES_params,TORQUES_params1] = getTorques(WAVE_params,HYDRO_params,WAVE_params1,OWSC_params1,OWSC_params,t1);
    
    
    pdf_struct(nn).Fpto1 = TORQUES_params1.Fpto1;
    pdf_struct(nn).v1 = HYDRO_params.v1;
    %% Save
    save(strcat(filename_prefix,'_',num2str(nn)))
    
    nn
    toc
end
%%
clearvars -except pdf_struct
save('pdf_struct.mat')