clear; close all; clc;
main_dir = 'G:\Dropbox (MREL)\MATLAB\Channel - Renzi';
filename_prefix = 'channel_h1';
cd(main_dir)
mkdir(filename_prefix);
folder = strcat(main_dir,'\',filename_prefix);
cd(folder)

%% Define parameters of sums and solving process
P = 4; % Upper limit of sums
N = 4; % Number of modes considered
M = 4;
u_num = 100;
j = 0:P; 
n = 0:N-1; % Vector of modes
v0j = cos(((2*j+1)*pi)/((2*P)+2)); % Calculate evaluation points
J = 100; % Upper limit for sub-series

%%%% Set simulation params structure
SIM_params.P = P;
SIM_params.N = N;
SIM_params.u_num = u_num;
SIM_params.M = M;
SIM_params.n = n;
SIM_params.v0j = v0j;
SIM_params.J = J;

%% Dimenionsional parameters
%%% Define wave and ocean paramenters (full-scale, dimentional) and fill
%%% WAVE_params structure
g = 9.81;
rho = 1000;
h1_vec = linspace(0.4, 0.7, 10); %0.7; % Depth of water

A01 = 0.1/2; % Incident wave amplitude [m] (between 5 and 15 cm)

T1 = 1.5; %4:14; % Period of wave
omega1 = 2*pi/T1; % Frequency of wave

%%%% Set WAVE params1 structure
WAVE_params1.g = g;
WAVE_params1.rho = rho;
% WAVE_params1.h1 = h1;
WAVE_params1.A01 = A01;
WAVE_params1.T1 = T1;
WAVE_params1.omega1 = omega1;

%%% Define OSWEC parameters (full-scale, dimensional)
w1 = 0.85; % Flap width
c1 = 0.15; % Height of foundation
a1 = 0.1; % Flap thickness
m_f = 11; % Mass of flap
m_w = 45; % Mass of displaced water (fully submerged)
Vol = 0.04584; % Volume of flap
Fg = m_f*g;
Fb = m_w*g;
db = 0.230; % Distance along flap to center of buoyancy
dg = 0.230; % Distance along flap to center of mass
% S1 = 3.025; % First moment of inertia (kg m)
% C1 = (w1*a1*((h1-c1)^2))-S1;
C1 = -(Fb*db-Fg*dg);
% C1 = 0.5;
I1 = 1.25;
b1 = 0.9;

%%%% Set OWSC params1 structure
OWSC_params1.w1 = w1;
OWSC_params1.c1 = c1;
OWSC_params1.C1 = C1;
OWSC_params1.I1 = I1;
OWSC_params1.b1 = b1;

A1 = b1/10; % I don't actually know what this is? Amplitude scale, must be much smaller than w1
WAVE_params1.A1 = A1;
eps = A1/b1;
WAVE_params1.eps = eps;

for i = 1:length(h1_vec)
    h1 = h1_vec(i);
    WAVE_params1.h1 = h1;
    
    %% Define time and space
    xnum = 10;
    ynum = 3;
    znum = 5;
    tnum = 50;

    %%% Dimensional time and space
    x1 = linspace(-1,1,xnum);
    y1 = linspace(-0.5*w1,0.5*w1,ynum);
    z1 = linspace(-h1,0,znum);
    t1 = linspace(0,T1,tnum);

    %%% Set dimensional structure
    XT_params1.x1 = x1;
    XT_params1.y1 = y1;
    XT_params1.z1 = z1;
    XT_params1.t1 = t1;

    %% Nondimensionalize parameters
    [OWSC_params,WAVE_params,XT_params] = nondimensionalize_channel(OWSC_params1,WAVE_params1,XT_params1);

    %% Get wavenumbers
    [WAVE_params] = getKappa(SIM_params,WAVE_params);

    %% Get Alpha and Beta
    [COEFF_params] = getAlphaBeta_channel(SIM_params,OWSC_params,WAVE_params);

    %% Get Hydro params (1)
    [HYDRO_params] = getHydro_channel(OWSC_params,WAVE_params,COEFF_params);

    %% PTO
    nu_PTO = HYDRO_params.nu_PTO_opt;
    HYDRO_params.nu_PTO = nu_PTO;

    %% Get Hydro params
    [HYDRO_params] = getHydro2_channel(HYDRO_params,OWSC_params,...
        WAVE_params,XT_params);
    [HYDRO_params1] = dimensionalizeHydro(HYDRO_params,OWSC_params1,WAVE_params1);

    %% Calculate torques
    [TORQUES_params(i)] = getTorques(WAVE_params,HYDRO_params,OWSC_params);
    [TORQUES_params1(i)] = dimensionalizeTorques(TORQUES_params(i),OWSC_params1,...
        WAVE_params1,XT_params1);
end




%%
save(filename_prefix)
