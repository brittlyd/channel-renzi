clear; close all; clc;
filename_prefix = 'channel_data_torques_c1_015';
folder = strcat('C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi\',filename_prefix);
cd(folder)

load pdf_struct.mat

nu_PTO = pdf_struct.nu_PTO;
bins_v1 = -1.5:0.1:1.5;
bins_Fpto1 = -150:10:150;

% Y_v1 = zeros(100,1); %length(pdf_struct)-1);
% Y_pto1 = zeros(100,1); %length(pdf_struct)-1);
N_struct = struct();
for j = 2:2:length(pdf_struct)
    v1 = pdf_struct(j).v1;
    Fpto1 = real(pdf_struct(j).Fpto1);
    [N,Xedges,Yedges] = histcounts2(v1,Fpto1,bins_v1,bins_Fpto1);
    
    N_struct(j).N = N;
    
    figure(j)
    pcolor(Xedges(1:end-1),Yedges(1:end-1),N./100)
    shading interp
    xlabel('V^1 [rad/x]')
    ylabel('F_{pto}^1 [Nm]')
    title(strcat('\nu_{PTO}=',num2str(nu_PTO(j))))
    colorbar
    caxis([0 0.1])
    saveas(gcf,strcat(filename_prefix,'_',num2str(j),'_jPDF.png'))
end