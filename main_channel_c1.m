clear; close all; clc;
cd('C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi');
filename_prefix = 'channel_c1_3';
mkdir(filename_prefix);
folder = strcat('C:\Users\britt\Dropbox (MREL)\MATLAB\Channel - Renzi\'...
    ,filename_prefix);
cd(folder)


%% Define parameters of sums and solving process
P = 4; % Upper limit of sums
N = 4; % Number of modes considered
M = 10;
u_num = 100;
j = 0:P; 
n = 0:N-1; % Vector of modes
v0j = cos(((2*j+1)*pi)/((2*P)+2)); % Calculate evaluation points
J = 100; % Upper limit for sub-series

%%%% Set simulation params structure
SIM_params.P = P;
SIM_params.N = N;
SIM_params.u_num = u_num;
SIM_params.M = M;
SIM_params.n = n;
SIM_params.v0j = v0j;
SIM_params.J = J;
    
c1_test = [0.1:0.05:0.4];

mu = zeros(1,length(c1_test));
F = zeros(1,length(c1_test));
nu = zeros(1,length(c1_test));
Theta1 = zeros(1,length(c1_test));
Theta1_mag = zeros(1,length(c1_test));
Theta1_phase = zeros(1,length(c1_test));
theta1 = zeros(100,length(c1_test));
CF = zeros(1,length(c1_test));

P1_struct = struct();

tic
for cc = 1:length(c1_test)
    %% Dimenionsional parameters
    g = 9.81;
    %%% Define OSWEC parameters (full-scale, dimensional)
    w1 = 0.8; % Flap width
    c1 = c1_test(cc); % Height of foundation
    m = 16; % Mass of flap
    m_w = 45; % Mass of displaced water (fully submerged)
    V = 0.04584; % Volume of flap
    Fg = m*g;
    Fb = m_w*g;
    db = 0.230; % Distance along flap to center of buoyancy
    dg = 0.230; % Distance along flap to center of mass
    C1 = (Fb*db-Fg*dg); 
    I1 = 1.25;
    b1 = 0.9;

    %%%% Set OWSC params1 structure
    OWSC_params1.w1 = w1;
    OWSC_params1.c1 = c1;
    OWSC_params1.C1 = C1;
    OWSC_params1.I1 = I1;
    OWSC_params1.b1 = b1;

    %%% Define wave and ocean paramenters (full-scale, dimentional) and fill
    %%% WAVE_params structure
    rho = 1000;
    h1 = 0.76; % Depth of water

    A01 = 0.1; % Incident wave height [m] (between 5 and 15 cm)
    A1 = b1/10; % I don't actually know what this is? Amplitude scale, must be much smaller than w1

    T1 = 1.5; %4:14; % Period of wave
    omega1 = 2*pi/T1; % Frequency of wave

    %%%% Set WAVE params1 structure
    WAVE_params1.g = g;
    WAVE_params1.rho = rho;
    WAVE_params1.h1 = h1;
    WAVE_params1.A01 = A01;
    WAVE_params1.A1 = A1;
    WAVE_params1.T1 = T1;
    WAVE_params1.omega1 = omega1;

    eps = A01/b1;

    %% Define time and space
    % xnum = 50;
    % ynum = 20;
    % znum = 20;
    tnum = 100;

    %%% Dimensional time and space
    % x1 = linspace(-2,2,xnum);
    % y1 = linspace(-0.5*b1,0.5*b1,ynum);
    % z1 = linspace(-h1,0,znum);
    t1 = linspace(0,5*T1,tnum);

    %%% Nondimensional time and space
    % x = x1/b1;
    % y = y1/b1;
    % z = z1/b1;
    t = t1/b1;

    %% Nondimensionalize parameters
    [OWSC_params,WAVE_params] = nondimensionalize_channel(OWSC_params1,...
        WAVE_params1);

    %% Get wavenumbers
    [WAVE_params] = getKappa(SIM_params,WAVE_params);

    %% Get Alpha and Beta
    [COEFF_params] = getAlphaBeta_channel(SIM_params,OWSC_params,...
        WAVE_params);

    %% Get Hydro params (1)
    [HYDRO_params] = getHydro_channel(OWSC_params,WAVE_params,...
        COEFF_params);
    mu(cc) = HYDRO_params.mu;
    F(cc) = HYDRO_params.F;
    nu(cc) = HYDRO_params.nu;

    %% PTO
    nu_PTO = HYDRO_params.nu_PTO_opt;
    HYDRO_params.nu_PTO = nu_PTO;

    %% Get Hydro params (2)
    [HYDRO_params] = getHydro2_channel(HYDRO_params,OWSC_params,...
        WAVE_params,OWSC_params1,WAVE_params1,t1);
    Theta1(cc) = HYDRO_params.Theta1;
    Theta1_mag(cc) = abs(Theta1(cc));
    Theta1_phase(cc) = imag(Theta1(cc))/real(Theta1(cc));
    theta1(:,cc) = HYDRO_params.theta1;
    CF(cc) = HYDRO_params.CF;
    
    %% Calculate torques
    [TORQUES_params,TORQUES_params1] = getTorques(WAVE_params,...
        HYDRO_params,WAVE_params1,OWSC_params1,OWSC_params,t1);
    
    Fe1 = real(TORQUES_params1.Fe1);
    Fr1 = real(TORQUES_params1.Fr1);
    Ft1 = real(TORQUES_params1.Ft1);
    Fb1 = real(TORQUES_params1.Fb1);
    Fpto1 = real(TORQUES_params1.Fpto1);
    P1_struct(cc).Fpto1 = TORQUES_params1.Fpto1;
    Fe1_vec(cc) = max(abs(Fe1));
    Fpto1_vec(cc) = max(abs(Fpto1));
    
    figure
    plot(t1,Fe1,'b','linewidth',1.5)
    hold on
    plot(t1,Fr1,'r','linewidth',1.5)
    % plot(t1,Ft1,'k','linewidth',1.5)
    plot(t1,Fb1,'g','linewidth',1.5)
    plot(t1,Fpto1,'m','linewidth',1.5)
    xlabel('t^1')
    legend('F_e','F_r','F_b','F_{PTO}')
    title(strcat('c^1=',num2str(c1)))
    saveas(gcf,strcat(filename_prefix,'_',num2str(cc),'_Torques.png'))
    
    Theta1 = HYDRO_params.Theta1;
    V1 = -1i*omega1*Theta1;
    V1_mag = abs(V1);
    V1_mag_vec(cc) = V1_mag;
    V1_phi = imag(V1)/real(V1);
    V1_t = V1_mag*exp(1i*V1_phi)*exp(-1i*omega1*t1);
    P1_struct(cc).V1_t = V1_t;

    figure
    yyaxis left
    plot(t1,real(V1_t),'linewidth',1.5)
    ylabel('V^1 (rad/s)')
    ylim([-1 1]*1.5)
    hold on
    yyaxis right
    plot(t1,real(Fpto1),'-','linewidth',1.5)
    ylabel('F^1 (N)')
    xlabel('t^1 (sec)')
    ylim([-1 1]*300)
    title(strcat('c^1=',num2str(c1)))
    saveas(gcf,strcat(filename_prefix,'_',num2str(cc),'_V_F_PTO.png'))
    
    
    
    %% Save
    save(strcat(filename_prefix,'_',num2str(cc)))
    
    toc
    cc
end
% mu1 = (rho*b1^5)*mu;
% F1 = (rho*g*A1*b1^3)*abs(F);
% nu1 = 

%%
figure(1);
plot(c1_test,(180/pi)*Theta1_mag,'ko','linewidth',1.5)
title('|\Theta^1|')
xlabel('c1 [m]')
ylabel('Degrees')
saveas(gcf,strcat(filename_prefix,'_Theta1.png'))

% figure(2);
% plot(c1_test,(180/pi)*Theta1_phase,'k','linewidth',1.5)
% title('\Theta Phase')
% xlabel('c1 [m]')
% ylabel('Degrees')

figure(2);
plot(c1_test,CF,'ko','linewidth',1.5)
title('Capture Factor')
xlabel('c1 [m]')
ylabel('%')
saveas(gcf,strcat(filename_prefix,'_CF.png'))

figure(3);
plot(c1_test,Fe1_vec,'ko','linewidth',1.5)
title('Excitation Torque')
xlabel('c1 [m]')
ylabel('[Nm]')
saveas(gcf,strcat(filename_prefix,'_Fe1.png'))

%%
P1 = zeros(length(t1),length(P1_struct));
P1_max = zeros(length(P1_struct),1);
for j = 1:length(P1_struct)
    P1(:,j) = P1_struct(j).V1_t.*P1_struct(j).Fpto1;
    P1_max(j) = max(real(P1(:,j)));
end

figure(4)
plot(c1_test,P1_max,'ko','linewidth',1.5)
xlabel('c1 [m]')
ylabel('P_{max} [W]')
title('Max Power')
saveas(gcf,strcat(filename_prefix,'_P1.png'))

%%
figure(5);
plot(c1_test,Fpto1_vec,'ko','linewidth',1.5)
title('PTO Torque')
xlabel('c1 [m]')
ylabel('[Nm]')
saveas(gcf,strcat(filename_prefix,'_Fpto1.png'))

figure(6);
plot(c1_test,V1_mag_vec,'ko','linewidth',1.5)
title('|V|^1')
xlabel('c1 [m]')
ylabel('[Nm]')
saveas(gcf,strcat(filename_prefix,'_V1.png'))

%%
save(filename_prefix)


